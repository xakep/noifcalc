package com.example.noifcalculator.Tests;

import com.example.noifcalculator.Services.Calculator;
import com.example.noifcalculator.Services.OperatorFactory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Fixture {

    private static final String ADD = "Add";
    private static final String SUB = "Subtract";
    private static final String MUL = "Multiply";
    private static final String DIV = "Divide";

    private OperatorFactory factory;
    private Calculator calc;

    @Before public void setup() {
        factory = new OperatorFactory();
        calc = new Calculator(factory);
    }

    @Test
    public void addTest() {
        calc.setDigit(20);
        calc.setOperator(ADD);
        calc.setDigit(30);
        calc.setOperator(ADD);
        calc.setDigit(40);

        Assert.assertEquals(90, calc.getResult());
    }

    @Test
    public void subTest() {
        calc.setDigit(20);
        calc.setOperator(SUB);
        calc.setDigit(30);
        calc.setOperator(SUB);
        calc.setDigit(40);

        Assert.assertEquals(-50, calc.getResult());
    }

    @Test
    public void mulTest() {
        calc.setDigit(2);
        calc.setOperator(MUL);
        calc.setDigit(2);
        calc.setOperator(MUL);
        calc.setDigit(2);

        Assert.assertEquals(8, calc.getResult());
    }

    @Test
    public void divTest() {
        calc.setDigit(200);
        calc.setOperator(DIV);
        calc.setDigit(2);
        calc.setOperator(DIV);
        calc.setDigit(20);

        Assert.assertEquals(5, calc.getResult());
    }

    @Test
    public void resultTest() {
        calc.setDigit(200);
        calc.setOperator(DIV);
        calc.setDigit(2);
        calc.setOperator(DIV);
        calc.setDigit(20);

        Assert.assertEquals(5, calc.getResult());

        calc.setOperator(ADD);
        calc.setDigit(10);
        calc.setOperator(MUL);
        calc.setDigit(2);

        Assert.assertEquals(25, calc.getResult());
    }

    @Test
    public void precedenceTest1() {
        calc.setDigit(2);
        calc.setOperator(ADD);
        calc.setDigit(2);
        calc.setOperator(MUL);
        calc.setDigit(2);

        Assert.assertEquals(6, calc.getResult());
    }

    @Test
    public void precedenceTest2() {
        calc.setDigit(200);
        calc.setOperator(SUB);
        calc.setDigit(100);
        calc.setOperator(DIV);
        calc.setDigit(20);

        Assert.assertEquals(195, calc.getResult());
    }
}
