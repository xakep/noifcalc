package com.example.noifcalculator.Models;

public class Multiply extends Expression {
    public Multiply(long val) {
        super(val);
    }

    public Multiply(Expression left) {
        super(left);
    }

    @Override
    public String getOperator() {
        return " * ";
    }

    @Override
    public long calculate() {
        this.fixPrecedence();

        return left.calculate();
    }

    @Override
    public Expression getExpression() {
        this.fixPrecedence();

        return left;
    }

    private void fixPrecedence() {
        long rigthArg = left.right.calculate();
        long newValue = rigthArg * right.calculate();

        left.right = new Number(newValue);
    }
}
