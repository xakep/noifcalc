package com.example.noifcalculator.Models;

public abstract class Expression {
    public Expression left;
    public Expression right;
    public long value = 0;

    public abstract String getOperator();
    public abstract long calculate() throws IllegalArgumentException;
    public abstract Expression getExpression();

    public Expression(long val) {
        this.value = val;
    }

    public Expression(Expression left) {
        this.left = left;
    }
}
