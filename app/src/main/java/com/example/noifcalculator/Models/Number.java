package com.example.noifcalculator.Models;

public class Number extends Expression {
    public Number(long val) {
        super(val);
    }

    public Number(Expression left) {
        super(left);
    }

    @Override
    public String getOperator() {
        return null;
    }

    @Override
    public long calculate() {
        return this.value;
    }

    @Override
    public Expression getExpression() {
        return this;
    }
}
