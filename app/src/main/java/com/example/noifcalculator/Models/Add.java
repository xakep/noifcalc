package com.example.noifcalculator.Models;

public class Add extends Expression {
    public Add(long val) {
        super(val);
    }

    public Add(Expression left) {
        super(left);
    }

    @Override
    public String getOperator() {
        return " + ";
    }

    @Override
    public long calculate() {
        return left.calculate() + right.calculate();
    }

    @Override
    public Expression getExpression() {
        return this;
    }
}
