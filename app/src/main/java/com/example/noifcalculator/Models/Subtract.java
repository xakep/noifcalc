package com.example.noifcalculator.Models;

public class Subtract extends Expression {
    public Subtract(long val) {
        super(val);
    }

    public Subtract(Expression left) {
        super(left);
    }

    @Override
    public String getOperator() {
        return " - ";
    }

    @Override
    public long calculate() {
        return left.calculate() - right.calculate();
    }

    @Override
    public Expression getExpression() {
        return this;
    }
}
