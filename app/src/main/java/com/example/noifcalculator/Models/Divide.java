package com.example.noifcalculator.Models;

public class Divide extends Expression {
    public Divide(long val) {
        super(val);
    }

    public Divide(Expression left) {
        super(left);
    }

    @Override
    public String getOperator() {
        return " / ";
    }

    @Override
    public long calculate() {
        this.fixPrecedence();

        return left.calculate();
    }

    @Override
    public Expression getExpression() {
        this.fixPrecedence();

        return left;
    }

    private void fixPrecedence() {
        long rightArg = left.right.calculate();
        long newValue = 0;
        try {
            newValue = rightArg / right.calculate();
        } catch (ArithmeticException ex) {
            throw new IllegalArgumentException();
        }
        left.right = new Number(newValue);
    }
}
