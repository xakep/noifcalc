package com.example.noifcalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.noifcalculator.Services.Calculator;
import com.example.noifcalculator.Services.OperatorFactory;

public class MainActivity extends Activity {

    private TextView textView;
    private Calculator calc;
    private int currentNumber = 0;
    private Button[] operators;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main);
        textView = (TextView) findViewById(R.id.txt_input);

        this.calc = new Calculator(new OperatorFactory()); // I'd use DI here

        this.operators = new Button[5];
        this.operators[0] = (Button) findViewById(R.id.btn_add);
        this.operators[1] = (Button) findViewById(R.id.btn_subtract);
        this.operators[2] = (Button) findViewById(R.id.btn_multiply);
        this.operators[3] = (Button) findViewById(R.id.btn_divide);
        this.operators[4] = (Button) findViewById(R.id.btn_enter);

        this.enableControls(false);
    }

    public void onDigitClick(View v) {
        this.enableControls(true);
        int input = Integer.parseInt(((Button) v).getText().toString());

        currentNumber = currentNumber * 10 + input;
        this.calc.setDigit(currentNumber);

        textView.setText(textView.getText() + (input + ""));
    }

    public void onOperatorClick(View v) {
        this.enableControls(false);
        this.currentNumber = 0;

        String tag = ((Button) v).getTag().toString();
        String op = this.calc.setOperator(tag);

        textView.setText(textView.getText() + op);
    }

    public void onGetResult(View v) {
        long result = 0;
        try {
            result = this.calc.getResult();
        } catch (IllegalArgumentException ex) {
            textView.setText("");
            textView.setHint("Division by zero is undefined");
            this.currentNumber = 0;
            return;
        }

        textView.setText(result + "");
        this.currentNumber = (int)result;
    }

    private void enableControls(boolean enabled) {
        for (int i = 0; i < this.operators.length; i++) {
            this.operators[i].setEnabled(enabled);
        }
    }
}
