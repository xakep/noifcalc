package com.example.noifcalculator.Services;

import com.example.noifcalculator.Models.Add;
import com.example.noifcalculator.Models.Divide;
import com.example.noifcalculator.Models.Expression;
import com.example.noifcalculator.Models.Multiply;
import com.example.noifcalculator.Models.Subtract;

import java.lang.reflect.Constructor;
import java.util.HashMap;

public class OperatorFactory {

    private static HashMap<String, Class> operators;

    static
    {
        operators = new HashMap<String, Class>();
        operators.put("Add", Add.class);
        operators.put("Subtract", Subtract.class);
        operators.put("Multiply", Multiply.class);
        operators.put("Divide", Divide.class);
    }

    public Expression create(String operator, Expression left) {
        try {
            Class op = operators.get(operator);
            Constructor ctr = op.getDeclaredConstructor(new Class[] { Expression.class });
            return ((Expression) ctr.newInstance(left));
        } catch (Exception ex) {
            return null;
        }
    }
}
