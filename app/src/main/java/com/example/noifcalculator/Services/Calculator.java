package com.example.noifcalculator.Services;

import com.example.noifcalculator.Models.Add;
import com.example.noifcalculator.Models.Expression;
import com.example.noifcalculator.Models.Number;

public class Calculator {

    private OperatorFactory factory;
    private Expression currentExpression;

    public Calculator(OperatorFactory factory) {
        this.factory = factory;
        this.currentExpression = new Add(new Number(0));
    }

    public void setDigit(int d) {
        currentExpression.right = new Number(d);
    }

    public String setOperator(String operator) {
        Expression left = currentExpression;
        left = left.getExpression();
        Expression right = this.factory.create(operator, left);
        currentExpression = right;

        return currentExpression.getOperator();
    }

    public long getResult() {
        long result = 0;
        try {
            result = currentExpression.calculate();
        }
        catch (IllegalArgumentException ex) {
            this.currentExpression = new Add(new Number(0));
            throw ex;
        }

        this.currentExpression = new Add(new Number(0));
        currentExpression.right = new Number(result);

        return result;
    }
}
